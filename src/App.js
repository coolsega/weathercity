import React from "react";  
import Info from "./components/info";  //подключаем компонеты
import Form from "./components/form"; 
import Weather from "./components/weather";

const API_KEY = "dd51a3fb40b07ec15c826cec41e7fdea"; //API ключ с сайта openweathermap.org

class App extends React.Component {

  state = {
    temp: undefined,
    city: undefined,
    country: undefined,
    pressure: undefined,
    sunrise: undefined,
    sunset: undefined,
    console: undefined,
    error: undefined
  }

  gettingWeather = async (event) => { 
    event.preventDefault(); //отключает перезагрузку страницы после нажатия кнопки
    var city = event.target.elements.city.value;
    
    if(city) {
      const api_url = await fetch (`https://api.openweathermap.org/data/2.5/weather?q=${city}&appid=${API_KEY}&units=metric`); //запрашиваем данные о погоде
      const data = await api_url.json ();
      console.log(data); //выводим данные о погоде в консоль
      
      let pressure = data.main.pressure; //преобразуем давление в мм рт. ст
      let pressureInMmHg = Math.floor(pressure * 0.75006);
      
      let sunriseInSec = data.sys.sunrise //преобразуем юникод время рассвета
      let datesunrise = new Date(sunriseInSec * 1000)
      let timeSunrise = datesunrise.toLocaleTimeString()

      let sunsetInSec = data.sys.sunset //преобразуем юникод время заката
      let date = new Date(sunsetInSec * 1000)
      let timeSunset = date.toLocaleTimeString()

      this.setState({
        temp: data.main.temp,
        city: data.name,
        country: data.sys.country,
        pressure: pressureInMmHg,
        sunrise: timeSunrise,
        sunset: timeSunset,
        error: undefined
      });
    } else {
      this.setState({
        temp: undefined,
        city: undefined,
        country: undefined,
        pressure: undefined,
        sunrise: undefined,
        sunset: undefined,
        error: "Введите название города"
      })
    }
  }

  render() {
    return (
      <div className="weather__app">
        <div className="main">
          <div className="container">
            <div className="row">
              <div className="info">
                <Info />
              </div>
              <div className="form">
                <Form weatherMethod={this.gettingWeather} />
                <Weather 
                  temp={this.state.temp}
                  city={this.state.city}
                  country={this.state.country}
                  pressure={this.state.pressure}
                  sunrise={this.state.sunrise}
                  sunset={this.state.sunset}
                  error={this.state.error}
                />
              </div>
            </div>
          </div>
        </div>
      </div> 
    );
  }
}

export default App;